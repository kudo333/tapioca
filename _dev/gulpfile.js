'use strict';

// Gulpと各種プラグインの読み込み
var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require("gulp-autoprefixer");
var csscomb = require('gulp-csscomb');
var plumber = require('gulp-plumber');
var imagemin = require('gulp-imagemin');
var cleancss = require('gulp-clean-css');
var notify = require('gulp-notify');

//imgとcssの圧縮
const paths = {
  'src': {  // 最適化前のファイル
      'img': '../_dev/src/images/**/*.+(jpg|jpeg|png|gif|svg)',
      'css': '../_dev/src/css/**/*.css'
  },
  'dist': {  // 最適化したファイルの出力先
      'img': '../images/',
      'css': '../css/'
  }
};

// Fileパスの設定
const filePath = {
  css: '../../common/css/',
  scss: '../scss/'
};

// // gulpのデフォルトタスクの指定
// gulp.task('default', ['watch']);

// ------------------------------------------
// Watch
// ファイルを監視し、指定したタスクを実行する
// ------------------------------------------


// 画像最適化
gulp.task('imagemin', function() {
  return gulp.src(paths.src.img)
  .pipe(plumber())
  .pipe(imagemin([
      pngquant({
          quality: [ 0.6, 0.8 ],
          speed: 1,
          floyd: 0
      }),
      mozjpeg({
          quality: 70,
          progressive: true
      }),
      imagemin.svgo(),
      imagemin.optipng(),
      imagemin.gifsicle()
  ]))
  .pipe(gulp.dest(paths.dist.img));
});

// cssコンパイル
gulp.task('compress-css', function() {
  return gulp.src(paths.src.css)
  .pipe(plumber())
  .pipe(cleancss())
  .pipe(gulp.dest(paths.dist.css));
});

// Sassのコンパイル
gulp.task('sass', function() {
  return gulp.src('./sass/**/*.scss')
    .pipe(plumber({errorHandler: notify.onError('<%= error.message %>')}))
    .pipe(sass({outputStyle: 'expanded'}))
    .pipe(gulp.dest('./css'));
});

// ------------------------------------------
// CSS
// SassをコンパイルしたのちCSSを整形する
// 
// gulp.src -> タスクを行う対象ファイルを指定
// gulp.dest -> タスク完了後、編集後のファイルの吐き出し先を指定
// 
// plugins.wait、plugins.plumber 
//  -> gulp-sassのコンパイルエラーを修正するために導入したプラグイン
// plugins.sass 
//  -> SassをCSSにコンパイルする
// plugins.autoprefixer
//  -> 指定したブラウザバージョンのベンダー接頭辞を付与してくれる
// plugins.groupCssMediaQueries
//  -> CSSに散見するメディアクエリをひとまとめにしてくれる
// plugins.csscomb
//  -> CSSを.csscomb.jsonの設定に基づき整形
// 
// ※ autoprefixerの対象バージョンは、案件ごとにきちんと指定する
// ------------------------------------------

// gulp.task('css', function () {
//   return gulp.src(filePath.scss + '*.scss')
//   .pipe(plugins.wait(500))
//   .pipe(plugins.plumber())
//   .pipe(
//       plugins.sass({
//           'style': 'expanded',
//           'includePaths': filePath.scss
//       }).on('error', plugins.sass.logError)
//   ).pipe(
//       plugins.autoprefixer({
//           browsers: ['last 1 versions'],
//           cascade: false
//       })
//   ).pipe(
//       plugins.groupCssMediaQueries()
//   ).pipe(
//       plugins.csscomb({
//           config: './.csscomb.json'
//       })
//   ).pipe(
//       gulp.dest(filePath.css)
//   );
// });

// // Watchタスク
// function watch() {
//   return gulp
//   .watch(paths.src.css)
//   .on('change', gulp.series('compress-css')); 
// }

// gulp.task('default', watch);

gulp.task('css', function () {
  return gulp.src(filePath.scss + 'master.scss')
  .pipe(plumber())
  .pipe( 
      sass({
          'style': 'expanded'
          // 'includePaths': filePath.scss
      }).on('error', sass.logError)
  ).pipe(
      autoprefixer({
          browsers: ['last 1 versions'],
          cascade: false
      })
  ).pipe(
      gulp.dest('css')
  );
});

// Watchタスク
function watch() {
  return gulp
  .watch(filePath.scss + 'master.scss')
  .on('change', gulp.series('css')); 
}

gulp.task('default', watch);








